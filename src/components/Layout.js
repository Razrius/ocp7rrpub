import React, {Component} from "react";
import MapContainer from "./MapContainer";
import Header from "./Header";
import RestaurantBrowser from "./RestaurantBrowser";

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import StarSelector from "./StarSelector";


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  divider: {
    heigth: "5px",
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Layout extends Component {
  constructor(props){
    super(props)
    this.state = {
      lat : this.props.lat,
      lng : this.props.lng,
    }
  }

  render() {
    const {classes} = this.props;
    return (
      <div>
        <Header />
        <div className={classes.root}>
          <Grid container spacing={24}>
            <Grid item className={classes.divider} sm={12}></Grid>
            <Grid item xs={12} sm={9}>
              <StarSelector />
              <MapContainer lat={this.state.lat} lng={this.state.lng} />
            </Grid>
            <Grid item xs={12} sm={3}>
              <Paper className={classes.paper}>
                <RestaurantBrowser />
              </Paper>
            </Grid>
          </Grid>
        </div>
      </div>
    )
  }
}


Layout.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Layout);