import React, { Component } from 'react';
import {observer, inject} from "mobx-react";

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider'

import AddReview from "./AddReview";


const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
});

class RestaurantBrowser extends Component {

  state = {
    name: 'Your Name',
    review: 'Your review',
    rating: 0,
    open: false,
    expanded: null,
    filter : this.props.filter
  };
  
  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  // Accesses MobX store's placeDetails observable item then crossreferences it with the passed props and finds the matching item then returns a list of reviews
  // If review is not available it provides a message
  getReviews(element) {
    let restaurant = element;
    let details = this.props.appStore.placeDetails;
    let result;

    for (let i=0; i<details.length; i++) {
      if (restaurant.place_id === details[i].place_id) {
        if (details[i].reviews) {
          result = details[i];
        }
      }
    }

    if (typeof(result) === "undefined" && result === undefined) {
      return (
        <div>
          Review is not available
        </div>
      )
    } else {

      return (
      <div>
        <List >
          <ListItem>
            <ListItemText primary={"Overall rating: " + result.rating}/>
          </ListItem>
          {result.reviews.map((props, index) => {
            return (
              <List dense key={index}>
                <ListItem>
                  <ListItemText primary={props.author_name} secondary={props.text}/>
                </ListItem>
                <ListItem>
                  <ListItemText primary={"Rating: " + props.rating}/>
                </ListItem>
                <Divider/>
              </List>
              )
            })}
          </List>
        </div>
      )
    }
  }

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;

    return (
      <div className={classes.root}>
        {this.props.appStore.filteredPlaces().map((element, index) =>(
          <ExpansionPanel expanded={expanded === element.id} onChange={this.handleChange(element.id)} key={index}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={classes.heading}>{element.name}</Typography>
              <Typography className={classes.secondaryHeading}>{element.vicinity}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              {this.getReviews(element)}
              <AddReview restaurant={element}/> 
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
      </div>
    );
  }
}

RestaurantBrowser.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(inject("appStore")(observer(RestaurantBrowser)));