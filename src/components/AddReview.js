import React, { Component } from 'react';
import {observer, inject} from "mobx-react";

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = theme => ({
  root: {
    width: '100%',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

class AddReview extends Component {

  state = {
    restaurant : this.props.restaurant,
    name: "",
    review: "",
    rating: 0,
    openDialog: false,
  };

  handleDialogClickOpen = () => {
    this.setState({ openDialog: true });
  };

  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };

  handleFormChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmitReview = () => {
    let restaurant = this.state.restaurant;
    let details = this.props.appStore.placeDetails;

    for (let i=0; i<details.length; i++) {
      if (restaurant.place_id === details[i].place_id) {
        this.props.appStore.placeDetails[i].reviews.push({ author_name: this.state.name, rating: this.state.rating, text: this.state.review})
        }
      }
    this.handleDialogClose();
  }

  render() {
    const { classes } = this.props;
    
    return (
      <div className={classes.root}>
        <Button variant="outlined" color="primary" onClick={this.handleDialogClickOpen}>
            Add a Review
        </Button>
        <Dialog
          open={this.state.openDialog}
          onClose={this.handleDialogClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add a Review</DialogTitle>
          <DialogContent>
            <DialogContentText>
              You can share your experience with the restaurant by writing a review about it. Your feedback is greatly appreciated.
            </DialogContentText>
            <TextField
              id="standard-with-placeholder"
              label="Your Name"
              placeholder="Your Name"
              className={classes.textField}
              onChange={this.handleFormChange('name')}
              value={this.state.name}
              margin="normal"
              fullWidth
            />
            <TextField
              id="standard-number"
              label="Rating"
              value={this.state.rating}
              onChange={this.handleFormChange('rating')}
              type="number"
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
              fullWidth
            />
            <TextField
              id="standard-multiline-flexible"
              label="Review"
              multiline
              rowsMax="4"
              value={this.state.review}
              onChange={this.handleFormChange('review')}
              className={classes.textField}
              margin="normal"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmitReview} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AddReview.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(inject("appStore")(observer(AddReview)));