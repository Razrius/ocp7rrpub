import React, { Component } from 'react';
import {observer, inject} from "mobx-react";

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


// Material UI style elements
const styles = theme => ({
  root: {
    width: '100%',
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing.unit * 20,
    left: theme.spacing.unit * 2,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

class AddRestaurant extends Component {

  state = {
    name: "",
    id: "",
    rating: 0,
    openDialog: false,
  };

  handleDialogClickOpen = () => {
    this.setState({ openDialog: true });
  };

  handleDialogClose = () => {
    this.setState({ openDialog: false });
  };

  handleFormChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  // Generates a random fake placeId in the absence of Googles placeId generator. This allows the submitted restaurant to appear on the right side panel
  // The component receives lat , lng position as props which is pushed into the new object
  handleSubmitRestaurant = () => {
    let placeId = 0;
    let calcRandom = Math.floor((Math.random() * 100) + 1);
    placeId = ( calcRandom * calcRandom - calcRandom ) * calcRandom;
    
    let data = [];
    data[0] = { 
      "geometry" : {"location": {"lat": this.props.lat, "lng": this.props.lng}}, 
      "name": this.state.name,
      "place_id" : placeId,
      "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png"
    }
    this.props.appStore.nearbyPlaces.push(data[0])
    
    this.handleDialogClose();
  }

  render() {
    const { classes } = this.props;
    
    return (
      <div className={classes.root}>
        <Fab color="primary" aria-label="Add" className={classes.fab} onClick={this.handleDialogClickOpen}>
          <AddIcon />
        </Fab>
        <Dialog
          open={this.state.openDialog}
          onClose={this.handleDialogClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add a new Restaurant</DialogTitle>
          <DialogContent>
            <DialogContentText>
              You can add a new resturant to the location you clicked the last time on the map.
            </DialogContentText>
            <TextField
              id="standard-with-placeholder"
              label="Name of the Restaurant"
              placeholder="Name of the Restaurant"
              className={classes.textField}
              onChange={this.handleFormChange('name')}
              value={this.state.name}
              margin="normal"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmitRestaurant} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AddRestaurant.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(inject("appStore")(observer(AddRestaurant)));