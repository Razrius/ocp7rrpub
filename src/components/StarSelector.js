import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {observer, inject} from "mobx-react";

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
});

class StarSelector extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
    this.props.appStore.setFilter(value);
  };

  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label="All Restaurants" />
          <Tab label="1 Star" />
          <Tab label="2 Stars" />
          <Tab label="3 Stars" />
          <Tab label="4 Stars" />
          <Tab label="5 Stars" />
        </Tabs>
      </Paper>
    );
  }
}

StarSelector.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(inject("appStore")(observer(StarSelector)));