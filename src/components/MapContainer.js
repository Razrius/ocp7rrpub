import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import {observer, inject} from "mobx-react";
import AddRestaurant from "./AddRestaurant";

const style = {
  width: '75%',
  height: '850px',
}

class MapContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      places : [],
      lat : this.props.lat,
      lng : this.props.lng,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      imageUrl: "",
      queryCounter: 0,
      clickedLat : 0,
      clickedLng : 0,
    };
  }

  // Searches for nearbyRestaurants based on the request values, then based on the generated list it looks for the reviews of each fetched item
  // Due to Google's QUERY LIMIT / Sec limitations needed to add a timer and batch requests in a batch of 10 items
  searchNearby = (mapProps, map) => {
    const { google } = mapProps;
    const center = new google.maps.LatLng(this.state.lat, this.state.lng);
    const service = new google.maps.places.PlacesService(map);
    
    // Specify location, radius and place types for your Places API search.
    const requestNearbyPlaces = {
      location: center,
      radius: '1500',
      type: ['restaurant']
    };
    
    const requestPlaceDetails = {
      placeId: "",
      fields: ['place_id', 'name', 'formatted_address', 'formatted_phone_number', 'rating', 'review']
    };

    let processedResults = [];
    let queryCount = 0;
  
    service.nearbySearch(requestNearbyPlaces, (results, status) => { 

      if (status === google.maps.places.PlacesServiceStatus.OK) {
        for(let i=0; i<results.length; i++) {;      
          let lat = results[i].geometry.location.lat();
          let lng = results[i].geometry.location.lng();
          processedResults.push(results[i]);
          processedResults[i].geometry.location.lat = lat;
          processedResults[i].geometry.location.lng = lng;
        }
        this.props.appStore.addNewPlace(processedResults);

        queryCount = 1 + processedResults.length / 10;
        this.setState({ queryCounter: queryCount})
      }

      var counter = 0;
      var batchCounter = 0;
      this.intervalId = setInterval(() => {
        
        this.setState({
          queryCounter: this.state.queryCounter - 1
        })
        if(this.state.queryCounter < 1) { 
          clearInterval(this.intervalId);
        }

        if(this.props.appStore.nearbyPlaces[counter].place_id) {
          for (let i = batchCounter; i<(batchCounter+10 && this.props.appStore.nearbyPlaces.length); i++) {
            requestPlaceDetails.placeId = this.props.appStore.nearbyPlaces[i].place_id;    
            service.getDetails(requestPlaceDetails, (results, status) => {
              if (status === google.maps.places.PlacesServiceStatus.OK) {
                this.props.appStore.addNewDetails(results);
                counter++;
              }
            })
          }
          batchCounter = batchCounter + 10;        
        }
      },5000)       
    });

  };

  // Triggers the InfoWindow to appear and pulls in the street view data of the location
  onMarkerClick = (props, marker, e) => {
    let streetView = this.imageSource(props)
    this.setState({
      selectedPlace: props,
      imageUrl : streetView,
      activeMarker: marker,
      showingInfoWindow: true
    })
  }    

  // Keeps track of the clicks on the Map, also stores click locations in the state
  onMapClicked = (mapProps, map, clickEvent) => {

    let lat = clickEvent.latLng.lat()
    let lng  = clickEvent.latLng.lng()
    
    this.setState({clickedLat : lat, clickedLng : lng})

    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  };

  // Fetches the street view photo of the location 
  imageSource (props) {

    let lat = props.position.lat;
    let lng = props.position.lng;

    let baseUrl = "https://maps.googleapis.com/maps/api/streetview?";
    let apiKey = "key=YOUR_API_KEY";
    let size = "size=300x300"

    let calculatedUrl = baseUrl + "location=" + lat.toString() + "," + lng.toString() + "&" + size + "&" + apiKey;
    return calculatedUrl;
  }

  render() {
   
    if (!this.props.loaded) return <div>Loading...</div>;
    return (
      <div>
        <Map google={this.props.google}
            onClick={this.onMapClicked}
            style={style}
            on
            onReady={this.searchNearby}
            initialCenter={{
              lat: this.props.lat,
              lng: this.props.lng,
            }}
            zoom={15}
            >
          <Marker onClick={this.onMarkerClick}
                  name={'Current location'}
                  position= {{lat: this.props.lat, lng: this.props.lng}}
          />
          <AddRestaurant lat={this.state.clickedLat} lng={this.state.clickedLng}/>
          {this.props.appStore.filteredPlaces().map((element, index) =>(
              <Marker onClick={this.onMarkerClick}
                name={element.name} 
                icon={{ url: element.icon, scaledSize: new this.props.google.maps.Size(30, 30)}}
                position= {{lat: element.geometry.location.lat, lng: element.geometry.location.lng}} 
                key={index}/>
          ))}
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}>
            <div>
              <h5>{this.state.selectedPlace.name}</h5>
              <img src={this.state.imageUrl} alt=""/>
            </div>
          </InfoWindow>
        </Map>
      </div>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: "YOUR_API_KEY"
})(inject("appStore")(observer(MapContainer)))