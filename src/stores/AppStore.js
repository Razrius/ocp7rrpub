import { action, decorate, observable } from "mobx";

class AppStore {
  
  nearbyPlaces = []
  filteredRestaurants = []
  filter = 0
  placeDetails = []

  setNearbyPlaces(data) {
    this.nearbyPlaces = data;
  }

  setFilter(value) {
    this.filter = value;
  }

  addNewPlace(data) {
    for (let i=0; i<data.length; i++) {
      this.nearbyPlaces.push(data[i]);
    }
  }

  addNewDetails(data) {
    this.placeDetails.push(data)
  }

  placeDetails(data) {
    let restaurant = data;
    let result = [];
    for (let i=0; i<this.placeDetails.length; i++) {
      if (this.placeDetails[i].place_id === restaurant.place_id) {
        result = this.placeDetails[i];
      }
    }
    return result;
  }

  filteredPlaces() {

    let ratingValue = this.filter;
    let filteredList = [];
    if (ratingValue !== 0) {
      for (let i=0; i<this.nearbyPlaces.length; i++) {
        if (this.nearbyPlaces[i].rating >= ratingValue && this.nearbyPlaces[i].rating < (ratingValue+1)) {
          filteredList.push(this.nearbyPlaces[i])
        }
      }
    } else {
      filteredList = this.nearbyPlaces;
    }
    this.filteredRestaurants = filteredList;
    return filteredList;
  }
} 

decorate(AppStore, {
  nearbyPlaces: observable,
  filteredRestaurants : observable,
  placeDetails : observable,
  filter : observable,
  setNearbyPlaces: action,
  setFilter: action,
  addNewPlace: action,
  addNewDetails: action,
})

export default new AppStore();