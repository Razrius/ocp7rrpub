import React, { Component } from "react";
import {geolocated} from "react-geolocated";
import CssBaseline from "@material-ui/core/CssBaseline";
import Layout from "./components/Layout";
import {observer, inject} from "mobx-react";

class App extends Component {

  // fetches the initial local JSON data and sets it in the MobX store's nearbyPlaces observable item
  fetchInitialValues() {
    fetch("/initialStore/initialRestaurants.json")
    .then(res => res.json())
    .then((result) => {
      this.props.appStore.setNearbyPlaces(result);    
    })
  }

  componentDidMount() {
    this.fetchInitialValues();
  }

  render() {

    return !this.props.isGeolocationAvailable
      ? <div>Your browser does not support Geolocation</div>
      : !this.props.isGeolocationEnabled
        ? <div>Geolocation is not enabled</div>
        : this.props.coords ?
        <React.Fragment>
          <CssBaseline />
          <Layout lat={this.props.coords.latitude} lng={this.props.coords.longitude} />
        </React.Fragment>

          : <div>Getting the location data&hellip; </div>;
  }
}

// Wraps the whole app with the geolocated Higher Order Component 
export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(inject("appStore")(observer(App)));

